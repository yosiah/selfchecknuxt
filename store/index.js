import Vuex from 'vuex'

const store = () =>
  new Vuex.Store({
    state: {
      coronaContact: { level: 0, desc: '' },
      coronaCase: { level: 0, desc: '' },
      caseType: {
        suspect: '의사환자(Suspect case)',
        investigation: '조사 대상 유증상자(Patients Under Investigation)'
      },
      levelType: {
        high: { code: 3, desc: '높음(High)' },
        middle: { code: 2, desc: '중간(Middle)' },
        low: { code: 1, desc: '낮음(Low)' }
      },
      degreeType: {
        high: { code: 3, degree: 37.5, desc: '37.5℃ 이상 발열' },
        middle: { code: 2, degree: 37.2, desc: '37.2 ~ 37.4 ℃ 발열' },
        low: { code: 1, degree: 0, desc: '37.2 미만 발열' }
      },
      coronaDecision: {
        contact: {
          YN: 'N',
          desc: '감염접촉: 확진환자와 접촉한 적이 있음'
        },
        transmission: {
          YN: 'N',
          desc: '감염접촉: 해외 방문했거나 집단발생과 역학적 연관 있음'
        },
        temperature: {
          checkYn: 'N',
          degree: 0,
          desc: '발열증상'
        },
        takingAntipyretics: {
          YN: 'N',
          desc: '복용약: 해열제'
        },
        takingPainkillers: {
          YN: 'N',
          desc: '복용약: 진통제'
        }
      },
      symptoms: {
        respiratory: ['dyspnea', 'cough'], // 호흡기
        pneumonia: {
          respiratory: true, // 호흡기
          YN: 'N',
          desc: '폐렴'
        },
        dyspnea: {
          respiratory: true,
          YN: 'N',
          desc: '이유없는 호흡곤란(숨가쁨)'
        },
        cough: {
          respiratory: true,
          YN: 'N',
          desc: '이유없는 기침(마른 기침)'
        },
        chills: {
          YN: 'N',
          desc: '이유없는 오한'
        },
        tiredness: {
          YN: 'N',
          desc: '이유없는 권태(몸이 피곤하고 나른함)'
        },
        sore_throat: {
          YN: 'N',
          desc: '이유없는 인후통(목구멍이 아픔)'
        },
        myalgia: {
          YN: 'N',
          desc: '이유없는 근육통'
        },
        headache: {
          YN: 'N',
          desc: '이유없는 두통(머리가 아픔)'
        },
        hemoptysis: {
          YN: 'N',
          desc: '이유없는 객혈(피를 토함)'
        },
        nausea: {
          YN: 'N',
          desc: '이유없는 오심(메스꺼움, 구역질)'
        },
        colic: {
          YN: 'N',
          desc: '이유없는 복통(배 아픔)'
        },
        diarrhea: {
          YN: 'N',
          desc: '이유없는 설사'
        },
        phlegm: {
          YN: 'N',
          desc: '이유없는 객담(가래)'
        }
      }
    },
    mutations: {
      increment(state) {
        state.counter++
      },
      getResult(state) {
        if (state.transmissionYn === 'Y') {
          state.coronaCase.desc = state.caseType.investigation
          state.coronaCase.level = state.levelType.high.code
          state.coronaContact.desc =
            '감염접촉: 해외 방문했거나 집단발생과 역학적 연관 있음'
          state.coronaContact.level = state.levelType.high.code
        } else if (state.coronaContactYn === 'Y') {
          state.coronaCase.desc = state.caseType.suspect
          state.coronaCase.level = state.levelType.high.code
          state.coronaContact.desc = '감염접촉: 확진환자와 접촉한 적이 있음'
          state.coronaContact.level = state.levelType.high.code
        } else {
          state.coronaCase.desc = ''
          state.coronaCase.level = ''
          state.coronaContact.desc = '감염접촉: 역학적 연관 없음'
          state.coronaContact.level = state.levelType.low.code
        }
      },
      setCoronaContactYn(state, v) {
        state.coronaDecision.contact.YN = v
      },
      setTransmissionYn(state, v) {
        state.coronaDecision.transmission.YN = v
      },
      setCheckTemperatureYn(state, v) {
        state.coronaDecision.temperature.checkYn = v
      },
      getTemperatureDesc(state) {
        let str = ''
        if (state.temperature >= 37.5) {
          str = '37.5℃ 이상 발열'
        } else if (state.temperature >= 37.2 && state.temperature < 37.5) {
          str = '37.2 ~ 37.4 ℃ 발열'
        } else {
          str = '37.2 미만 발열'
        }
        return str
      },
      setTemperature(state, v) {
        state.coronaDecision.temperature.degree = v
      },
      setTakingAntipyreticsYn(state, v) {
        state.coronaDecision.takingAntipyretics.YN = v
      },
      setTakingPainkillersYn(state, v) {
        state.coronaDecision.takingPainkillers.YN = v
      },
      getSymptomsDesc(state) {
        const arrStr = []
        if (state.symptoms.pneumonia === 'Y') {
          arrStr.push('폐렴')
        }
        if (state.symptoms.dyspnea === 'Y') {
          arrStr.push('이유없는 호흡곤란(숨가쁨)')
        }
        if (state.symptoms.cough === 'Y') {
          arrStr.push('이유없는 기침(마른 기침)')
        }
        if (state.symptoms.respiratory === 'Y') {
          arrStr.push('호흡기 질환이 있는 것으로 파악됨')
        }
        if (state.symptoms.chills === 'Y') {
          arrStr.push('이유없는 오한')
        }
        if (state.symptoms.tiredness === 'Y') {
          arrStr.push('이유없는 권태(몸이 피곤하고 나른함)')
        }
        if (state.symptoms.sore_throat === 'Y') {
          arrStr.push('이유없는 인후통(목구멍이 아픔)')
        }
        if (state.symptoms.myalgia === 'Y') {
          arrStr.push('이유없는 근육통')
        }
        if (state.symptoms.headache === 'Y') {
          arrStr.push('이유없는 두통(머리가 아픔)')
        }
        if (state.symptoms.hemoptysis === 'Y') {
          arrStr.push('이유없는 객혈(피를 토함)')
        }
        if (state.symptoms.nausea === 'Y') {
          arrStr.push('이유없는 오심(메스꺼움, 구역질)')
        }
        if (state.symptoms.colic === 'Y') {
          arrStr.push('이유없는 복통(배 아픔)')
        }
        if (state.symptoms.diarrhea === 'Y') {
          arrStr.push('이유없는 설사')
        }
        if (state.symptoms.phlegm === 'Y') {
          arrStr.push('이유없는 객담(가래)')
        }

        return arrStr.toString()
      },
      setSymptoms(state, v) {
        // console.log(v)
        for (const key in v) {
          state.symptoms[key].YN = v[key]
        }
      }
    }
  })
export default store
